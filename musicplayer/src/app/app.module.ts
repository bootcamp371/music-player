import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { PlayerComponent } from './player/player.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { SongsService } from './providers/songs.service';


const appRoutes: Routes = [
  
  { path: "", component: PlaylistComponent },
  { path: "player", component: PlayerComponent }
  
];
@NgModule({
  declarations: [
    AppComponent,
    PlayerComponent,
    PlaylistComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [SongsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
