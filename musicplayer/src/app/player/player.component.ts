import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { SongsService } from '../providers/songs.service';
import { Songs } from '../models/songs.model';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent {
  song!: any;
  url!: SafeResourceUrl;
  songs: Array<Songs> = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private mySongService: SongsService
  ) { }

  ngOnInit() {
    this.songs = this.mySongService.getSongsList();
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];

      //this.song = this.getSong(id);
      this.song = this.mySongService.getSongThroughService(id);
      this.url =
        this.sanitizer.bypassSecurityTrustResourceUrl(this.song.src);
    });
  }

  getSong(id: number) {
    return this.songs.find((song) => song.id == id);
  }

}
