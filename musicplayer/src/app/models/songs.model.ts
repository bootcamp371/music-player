export class Songs {

    id: number = 0;
    name: string = "";
    artist: string = "";
    cover: string = "";
    src: string = "";

    constructor (id: number, name: string, artist: string, cover: string, src: string){
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.cover = cover;
        this.src = src;
    }
}
