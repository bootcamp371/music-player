import { Injectable } from '@angular/core';
import { Songs } from '../models/songs.model';

@Injectable({
  providedIn: 'root'
})
export class SongsService {
  songs: Array<Songs> = [];

  constructor() {
    this.songs = [
      new Songs(1, "Billie Jean", "Michael Jackson", "assets/images/Thriller.jpg", "https://www.youtube.com/embed/Zi_XLOBDo_Y"),
      new Songs(2, "Livin' on a Prayer", "Bon Jovi", "assets/images/SlipperyWhenWet.jpg", "https://www.youtube.com/embed/lDK9QqIzhwk"),
      new Songs(3, "Sweet Child o' Mine", "Guns N' Roses", "assets/images/AppetiteForDestruction.jpg", "https://www.youtube.com/embed/1w7OgIMMRc4"),
      new Songs(4, "Take On Me", "a-ha", "assets/images/TakeOnMe.jpg", "https://www.youtube.com/embed/djV11Xbc914"),
      new Songs(5, "Like a Virgin", "Madonna", "assets/images/LikeAVirgin.jpg", "https://www.youtube.com/embed/s__rX_WL100"),
      new Songs(6, "Armageddon It", "Def Leppard", "assets/images/Hysteria.jpg", "https://www.youtube.com/embed/hHL2Fc0XD50")
    ];
  }

  public getSongsList(): Array<Songs>{
    return this.songs;
  }
  public getSongThroughService(id: number): Songs | undefined{
    return this.songs.find((song) => song.id == id);
  }
}
