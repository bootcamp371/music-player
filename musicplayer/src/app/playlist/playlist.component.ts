import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SongsService } from '../providers/songs.service';
import { Songs } from '../models/songs.model';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {
  constructor(private router: Router, private mySongService: SongsService ) {}
  songs: Array<Songs> = [];

  ngOnInit(): void {
    this.songs = this.mySongService.getSongsList();
  }

  playSong(idInput:string):void{
    this.router.navigate(['/player'],
    {
    queryParams: {
    id: idInput
    }
    }
    );
  }
}
